// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access Firestore.
const admin = require('firebase-admin');
admin.initializeApp();

exports.hitos = functions.https.onRequest((req, res) => {
    var stuff = [];
    var jsonBody = {}
    jsonBody.stand = {}
    var db = admin.firestore();
    db.collection("hitos").get().then(snapshot => {
       /*  var year70 = []
        snapshot.forEach(doc => {
            if (doc.data().category == "AUTORIDADES UTPL") {
                let newelement = {
                    "id": doc.id,
                    "title": doc.data().title,
                    "category": doc.data().category,
                    "date": doc.data().date,
                    "image": doc.data().image,
                    "video": doc.data().video ? doc.data().video : "",
                }
                year70 = year70.concat(newelement);
            }
        });
        var institutional_pride = []
        snapshot.forEach(doc => {
            if (doc.data().category == "Orgullo Institucional") {
                let newelement = {
                    "id": doc.id,
                    "title": doc.data().title,
                    "category": doc.data().category,
                    "date": doc.data().date,
                    "image": doc.data().image
                }
                institutional_pride = institutional_pride.concat(newelement);
            }
        });
        var production_plants = []
        snapshot.forEach(doc => {
            if (doc.data().category == "Plantas productivas UTPL") {
                let newelement = {
                    "id": doc.id,
                    "title": doc.data().title,
                    "category": doc.data().category,
                    "date": doc.data().date,
                    "image": doc.data().image
                }
                production_plants = production_plants.concat(newelement);
            }
        });
        var curious_facts = []
        snapshot.forEach(doc => {
            if (doc.data().category == "Datos Curiosos") {
                let newelement = {
                    "id": doc.id,
                    "title": doc.data().title,
                    "category": doc.data().category,
                    "date": doc.data().date,
                    "image": doc.data().image
                }
                curious_facts = curious_facts.concat(newelement);
            }
        }); */
        jsonBody.stand = {
            ...{ year70: year70 },
        };
        res.send(jsonBody)
        return "";
    }).catch(reason => {
        res.send(reason)
    })
});
/*
var year80 = []
snapshot.forEach(doc => {
    let date = new Date(doc.data().date.seconds * 1000).getUTCFullYear()
    if (date >= 1970 || date < 1980) {
        let newelement = {
            "id": doc.id,
            "title": doc.data().title,
            "category": doc.data().category,
            "description": doc.data().description,
            "date": new Date(doc.data().date.seconds * 1000).getUTCFullYear(),
            "image": doc.data().image,
            "video": doc.data().video ? doc.data().video : '',
        }
        year80 = year80.concat(newelement);
    }
});
*/