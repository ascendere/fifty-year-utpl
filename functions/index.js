// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const cors = require('cors')({ origin: true });

// The Firebase Admin SDK to access Firestore.
const admin = require('firebase-admin');
admin.initializeApp();

exports.hitos = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        // your function body here - use the provided req and res from cors
        var stuff = [];
        var jsonBody = {}
        jsonBody.stand = {}
        var db = admin.firestore();
        db.collection("hitos").orderBy('date', 'asc').get().then(snapshot => {
            var year70 = []
            var year80 = []
            var year90 = []
            var year2000 = []
            var year2010 = []
            var year2020 = []

            snapshot.forEach(doc => {
                if (doc.data().date >= 1970 && doc.data().date <= 1979) {
                    console.log(doc.id)
                    let newelement = {
                        "id": doc.id,
                        "title": doc.data().title,
                        "category": "Década de los 70'",
                        "description": doc.data().description,
                        "date": doc.data().date,
                        "image": doc.data().image ? doc.data().image : "",
                        "video": doc.data().video ? doc.data().video : "",
                    }
                    year70 = year70.concat(newelement);
                }
                if (doc.data().date >= 1980 && doc.data().date <= 1989) {
                    let newelement = {
                        "id": doc.id,
                        "title": doc.data().title,
                        "category": "Década de los 80'",
                        "description": doc.data().description,
                        "date": doc.data().date,
                        "image": doc.data().image ? doc.data().image : "",
                        "video": doc.data().video ? doc.data().video : "",
                    }
                    year80 = year80.concat(newelement);
                }
                if (doc.data().date >= 1990 && doc.data().date <= 1999) {
                    let newelement = {
                        "id": doc.id,
                        "title": doc.data().title,
                        "category": "Década de los 90'",
                        "description": doc.data().description,
                        "date": doc.data().date,
                        "image": doc.data().image ? doc.data().image : "",
                        "video": doc.data().video ? doc.data().video : "",
                    }
                    year90 = year90.concat(newelement);
                }
                if (doc.data().date >= 2000 && doc.data().date <= 2009) {
                    let newelement = {
                        "id": doc.id,
                        "title": doc.data().title,
                        "category": "Década de los 2000'",
                        "description": doc.data().description,
                        "date": doc.data().date,
                        "image": doc.data().image ? doc.data().image : "",
                        "video": doc.data().video ? doc.data().video : "",
                    }
                    year2000 = year2000.concat(newelement);
                }
                if (doc.data().date >= 2010 && doc.data().date <= 2019) {
                    let newelement = {
                        "id": doc.id,
                        "title": doc.data().title,
                        "category": "Década de los 2010'",
                        "description": doc.data().description,
                        "date": doc.data().date,
                        "image": doc.data().image ? doc.data().image : "",
                        "video": doc.data().video ? doc.data().video : "",
                    }
                    year2010 = year2010.concat(newelement);
                }
                if (doc.data().date >= 2020 && doc.data().date <= 2029) {
                    let newelement = {
                        "id": doc.id,
                        "title": doc.data().title,
                        "category": "Década de los 2020'",
                        "description": doc.data().description,
                        "date": doc.data().date,
                        "image": doc.data().image ? doc.data().image : "",
                        "video": doc.data().video ? doc.data().video : "",
                    }
                    year2020 = year2020.concat(newelement);
                }
            });

            jsonBody.stand = {
                ...{ year70: year70 },
                ...{ year80: year80 },
                ...{ year90: year90 },
                ...{ year2000: year2000 },
                ...{ year2010: year2010 },
                ...{ year2020: year2020 },
            };
            res.send(jsonBody)
            return "";
        }).catch(reason => {
            res.send(reason)
        })
    })

});