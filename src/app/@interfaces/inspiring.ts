export interface Inspiring {
    ci: string;
    degree: string;
    type: string;
    nameTeacher: string;
    message: string;
}
export interface Degree {
    name: string;
}
