export interface User {
    uid?: string;
    id?: string;
    businessPhones?: string[];
    displayName?: string; // UTPL
    mail?: string; // UTPL
    surname?: string; // UTPL
    givenName?: string; // UTPL
    email?: string; // Gmail
    jobTitle?: string; // UTPL
    role?: string;
}
