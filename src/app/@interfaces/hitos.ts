import { User } from './user';
export interface Hitos {
    id: string;
    title: string;
    description: string;
    date: Date;
    image?: string;
    category: string;
    video?: string;
    created: User;
}