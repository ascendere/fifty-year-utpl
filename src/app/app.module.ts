import { MatNativeDateModule } from '@angular/material/core';
import { MaterialModule } from './material-module';
import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './@pages/home/home.component';

import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';

import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { HitosComponent } from './@pages/hitos/hitos.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { registerLocaleData } from '@angular/common';
import { PortalComponent } from './@pages/admin/portal/portal.component';
import { UnityComponent } from './@component/unity/unity.component';
import { TourComponent } from './@pages/tour/tour.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HitosComponent,
    PortalComponent,
    UnityComponent,
    TourComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireStorageModule,
    AngularFirestoreModule,
    FormsModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    MatNativeDateModule

  ],
  providers: [
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'outline' } },
   /*  { provide: LOCALE_ID, useValue: 'es-Ar' }, */
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
