import { User } from './../@interfaces/user';
import { AuthService } from './../@services/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { take, map, tap } from 'rxjs/operators';
import Swal from 'sweetalert2/dist/sweetalert2.js';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  user: User;
  constructor(private authService: AuthService, private router: Router) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.authService.isLoggedIn !== true && this.authService.isAdmin !== true) {
      /* Swal.fire({
        title: "Acceso denegado",
      }) */
      this.authService.MicrosoftAuth().then(() => {
        console.log("louegado")
      })
      /*  this.router.navigate(['']) */
    }
    return true;
  }

}
