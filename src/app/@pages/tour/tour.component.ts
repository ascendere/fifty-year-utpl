import { Component, OnInit, ViewChild } from '@angular/core';

declare var createUnityInstance: any;
@Component({
  selector: 'app-tour',
  templateUrl: './tour.component.html',
  styleUrls: ['./tour.component.scss']
})
export class TourComponent implements OnInit {
  @ViewChild('unityView', { static: true }) unityView;

  constructor() { }

  ngOnInit(): void {
  }
  load() {

  }
}
