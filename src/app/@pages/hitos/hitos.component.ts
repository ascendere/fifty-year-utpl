import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { Categories } from './../../@interfaces/categories';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { FormGroup, FormControl, Validators, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Hitos } from './../../@interfaces/hitos';
import { HitosService } from './../../@services/hitos.service';
import { Component, forwardRef, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';
import { User } from 'src/app/@interfaces/user';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

export interface UserData {
  id: string;
  name: string;
  progress: string;
  color: string;
}

/** Constants used to fill up our data base. */
const COLORS: string[] = [
  'maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple', 'fuchsia', 'lime', 'teal',
  'aqua', 'blue', 'navy', 'black', 'gray'
];
const NAMES: string[] = [
  'Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack', 'Charlotte', 'Theodore', 'Isla', 'Oliver',
  'Isabella', 'Jasper', 'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'
];


import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { defaultFormat as _rollupMoment, Moment } from 'moment';

const moment = _rollupMoment || _moment;
/* --- */
export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  },
};
export const YEAR_MODE_FORMATS = {
  parse: {
    dateInput: 'YYYY',
  },
  display: {
    dateInput: 'YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
@Component({
  selector: 'app-hitos',
  templateUrl: './hitos.component.html',
  styleUrls: ['./hitos.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'pt' },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
    { provide: MAT_DATE_FORMATS, useValue: YEAR_MODE_FORMATS },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HitosComponent),
      multi: true,
    },
  ]
})
export class HitosComponent implements OnInit {
  categories: Categories[] = [];
  hito = {} as Hitos
  hitos = [];
  myFiles: string[] = [];
  uploadPercent: Observable<number>;
  chosenYearDate: Date;

  formHito = new FormGroup({
    title: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    date: new FormControl('', [Validators.required]),
    category: new FormControl('', [Validators.required]),
  });

  displayedColumns: string[] = [/* 'category', */ 'title', 'description', 'date', 'image', 'video', /* 'created', */ 'edit'];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(public hitosService: HitosService, private storage: AngularFireStorage, private sanitizer: DomSanitizer) {
    this.hitosService.findAllHitos().subscribe(hitos => {
      this.hitos = hitos;
      this.dataSource = new MatTableDataSource(this.hitos);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
    // Assign the data to the data source for the table to render
  }
  ngAfterViewInit() {

  }

  handleKeyUp(e, after, type, hito: Hitos) {
    const before = (e.target as HTMLInputElement).value
    if (e.keyCode === 13) {
      if (before !== after) {
        Swal.fire({
          title: "¿Seguro que desea modificar?",
          html: `<p><strong>Antes:</strong> ${after} </p>
                 <p><strong>Ahora:</strong> ${before} </p>`,
          showDenyButton: true,
          confirmButtonText: `Si`,
          denyButtonText: `No`,
        }).then((result) => {
          if (result.isConfirmed) {
            this.hitosService.updateHito(before, type, hito.id).then(() => {
              Swal.fire('Cambios realizados', '', 'success')
            })
          } else if (result.isDenied) {
            Swal.fire('No se ha realizado cambios', '', 'info')
          }
        })
      }

    }
  }
  sanitizeImageUrl(imageUrl: string): SafeUrl {
    return this.sanitizer.bypassSecurityTrustUrl(imageUrl);
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit(): void {
    this.hitosService.findCategories().subscribe(category => {
      this.categories = category;
    })
  }
  get f() {
    return this.formHito.controls;
  }
  onFileChange(event) {
    for (var i = 0; i < event.target.files.length; i++) {
      this.myFiles.push(event.target.files[i]);
    }
  }
  onUploadVideo(e, hito: Hitos) {
    this.hito = hito;
    const id = Math.random().toString(36).substring(2);
    const file = e.target.files[0];
    const filePath = `videos-hitos/${id}`;
    const ref = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    this.uploadPercent = task.percentageChanges();
    console.log(this.uploadPercent)
    task.snapshotChanges().pipe(finalize(() => {
      ref.getDownloadURL().subscribe(urlVideo => {
        this.hito.video = urlVideo;
        this.hitosService.updateVideo(this.hito).then(() => {
          Swal.fire({
            text: 'Video Actualizado',
            timer: 200
          })
        })
      });
    })
    ).subscribe();
  }
  onUploadImage(e, hito: Hitos) {
    this.hito = hito;
    const id = Math.random().toString(36).substring(2);
    const file = e.target.files[0];
    const filePath = `images-hitos/${id}`;
    const ref = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges().pipe(finalize(() => {
      ref.getDownloadURL().subscribe(urlImage => {
        this.hito.image = urlImage;
        this.hitosService.updateImg(this.hito).then(() => {
          Swal.fire({
            text: 'Imagen Actualizada',
            timer: 200
          })
        })
      });
    })
    ).subscribe();
  }
  submit() {
    const user = JSON.parse(localStorage.getItem('user')) as User;
    if (this.formHito.invalid) {
      return
    }
    this.hito.video = this.hito.video ? this.hito.video : null;
    this.hito.image = this.hito.image ? this.hito.image : null;
    this.hitosService.addHito({
      image: this.hito.image,
      video: this.hito.video,
      ...this.formHito.value,
      created: { name: user.displayName, email: user.email, date: new Date() }
    }).then(() => {
      Swal.fire({
        title: "Hito registrado",
      })
    })
  }
}
