import { Observable } from 'rxjs';
import { InspiringService } from './../../@services/inspiring.service';
import { Inspiring, Degree } from './../../@interfaces/inspiring';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2'
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  teacherCount: number = 0;
  studentCount: number = 0;
  alumniCount: number = 0;
  flickerResponse = [
    "https://www.utpl.edu.ec/sites/default/files/2018/hitos/hito-1971.jpg",
    "../../../assets/banner/banner-2.jpeg",
    "../../../assets/banner/banner-3.jpeg",
    "https://www.utpl.edu.ec/sites/default/files/2018/hitos/hito-3-1976.jpg",
    "../../../assets/banner/banner-4.jpeg",
    "../../../assets/banner/banner-5.jpeg",
    "../../../assets/banner/banner-6.jpeg",
    "../../../assets/banner/banner-7.jpeg",
    "https://www.utpl.edu.ec/sites/default/files/2018/hitos/hito-4-1983.jpg"
  ]
  form: FormGroup;
  inspiringData: Inspiring;
  options: Inspiring[] = [];
  optionDegree: Degree[] = [];
  filteredOptions: Observable<Inspiring[]>;
  filteredDegree: Observable<Degree[]>;

  constructor(private inspiringService: InspiringService) {

  }

  ngOnInit(): void {
    this.form = new FormGroup({
      ci: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
      degree: new FormControl('', [Validators.required,]),
      type: new FormControl('', Validators.required),
      nameTeacher: new FormControl('', Validators.required),
      message: new FormControl('')
    });
    this.options = []
    this.inspiringService.getDegree().subscribe(degree => {
      this.optionDegree = degree
    })
    this.inspiringService.getInspiring().subscribe(inspiring => {
      this.options = inspiring
      const filterTeacher = this.options.reduce((acc, current) => {
        const x = acc.find(item => item.nameTeacher === current.nameTeacher);
        if (!x) {
          return acc.concat([current]);
        } else {
          return acc;
        }
      }, []);
      this.teacherCount = filterTeacher.length
      this.alumniCount = 0;
      this.studentCount = 0;
      const filterStudent = this.options.reduce((acc, current) => {
        const x = acc.find(item => item.ci === current.ci);
        if (!x) {
          return acc.concat([current]);
        } else {
          return acc;
        }
      }, []);
      /*  this.studentCount = filterStudent.length
       console.log(filterStudent.length) */
      filterStudent.forEach(element => {
        if (element.type == 'alumni')
          this.alumniCount = this.alumniCount + 1;
        else
          this.studentCount = this.studentCount + 1;
      });
    })
    /* this.inspiringService.getType('alumni').subscribe(alumni=>{
      console.log(alumni.length)
    })
    this.inspiringService.getType('estudiante').subscribe(alumni=>{
      console.log(alumni.length)
    }) */
    /* this.inspiringService.getInspiring2().subscribe(inspiring => {
      console.log(inspiring.length)
    }) */

    this.filteredOptions = this.form.get('nameTeacher').valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
    this.filteredDegree = this.form.get('degree').valueChanges.pipe(
      startWith(''),
      map(value => this._filterDegree(value))
    );

  }
  private _filter(value: string): Inspiring[] {
    const filterValue = value.toLowerCase();
    const filteredArr = this.options.reduce((acc, current) => {
      const x = acc.find(item => item.nameTeacher === current.nameTeacher);
      if (!x) {
        return acc.concat([current]);
      } else {
        return acc;
      }
    }, []);
    return filteredArr.filter(option => option.nameTeacher.toLowerCase().indexOf(filterValue) === 0);
  }
  private _filterDegree(value: string): Degree[] {
    const filterValue = value.toLowerCase();
    return this.optionDegree.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0);
  }
  get formInspiring() {
    return this.form;
  }
  submit() {
    /* this.form.get('nameTeacher').setValue(this.myControl.value) */
    if (this.form.invalid) {
      Swal.fire({
        title: 'Falta información',
        text: 'Deberá llenar todo el formulario',
        icon: 'error',
      })
      return;
    }
    this.inspiringData = this.form.value;
    console.log(this.inspiringData)
    this.inspiringService.addInspiring(this.inspiringData).then(() => {
      Swal.fire({
        html: '<div><strong style="color:#003f72;">Gracias por registrar tu respuesta, significará mucho para aquellos docentes con vocación al servicio de la enseñanza de calidad</div>',
        footer: '<a href>¿Necesitas ayuda?</a>',
        showDenyButton: true,
        confirmButtonText: `¿Quieres registrar a otro docente?`,
        denyButtonText: `No, gracias`,
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          this.form.get('message').setValue('')
          this.form.get('nameTeacher').setValue('')
        } else if (result.isDenied || result.isDismissed) {
          Swal.fire({
            title: 'Gracias por registrar tu participación',
          })
          this.form.reset()
        }
      })

    })
  }

}
