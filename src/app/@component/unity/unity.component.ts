import { AuthService } from './../../@services/auth.service';
import { Router } from '@angular/router';
import { Component, Input, OnInit, NgZone, HostListener } from '@angular/core';


@Component({
  selector: 'app-unity',
  templateUrl: './unity.component.html',
  styleUrls: ['./unity.component.scss']
})
export class UnityComponent implements OnInit {
  gameInstance: any;
  progress = 0;
  isReady = false;
  constructor(
    private ngZone: NgZone,
    private router: Router,
    public authService: AuthService,
  ) { }


  ngOnInit() {
    const loader = (window as any).UnityLoader;

    this.gameInstance = loader.instantiate(
      'gameContainer',
      '/assets/BuildLabQuimica/Build/BuildLabQuimica.json', {
      onProgress: (gameInstance: any, progress: number) => {
        this.progress = progress;
        if (progress === 1) {
          this.isReady = true;
        }
      }
    });
  }

}
