import { Categories } from './../@interfaces/categories';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Hitos } from '../@interfaces/hitos';
import { finalize, map } from 'rxjs/operators';
import { AngularFireStorage } from '@angular/fire/storage';

@Injectable({
  providedIn: 'root'
})
export class HitosService {

  uploadPercent: Observable<number>;
  hitosCollection: AngularFirestoreCollection;
  hitos: Observable<Hitos[]>;
  hito = {} as Hitos;
  COLLECTION_NAME = 'hitos'


  constructor(private db: AngularFirestore, private storage: AngularFireStorage) {
    this.hitosCollection = this.db.collection(this.COLLECTION_NAME, ref => ref.orderBy('date', 'asc'));
    this.hitos = this.hitosCollection.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Hitos;
        data.id = a.payload.doc.id;
        return data;
      });
    }));
  }

  addHito(hito: Hitos) {
    this.hitosCollection = this.db.collection(this.COLLECTION_NAME);
    return this.hitosCollection.add(hito)
  }
  updateHito(field: any, type, id) {
    if (type === 'title') {
      return this.db.collection(this.COLLECTION_NAME).doc(id).set(
        {
          title: field
        },
        {
          merge: true,
        }
      )
    }
    if (type === 'description') {
      return this.db.collection(this.COLLECTION_NAME).doc(id).set(
        {
          description: field
        },
        {
          merge: true,
        }
      )
    }
    if (type === 'date') {
      return this.db.collection(this.COLLECTION_NAME).doc(id).set(
        {
          date: field
        },
        {
          merge: true,
        }
      )
    }

  }
  updateImg(hito: Hitos) {
    return this.db.collection(this.COLLECTION_NAME).doc(hito.id).set(
      {
        image: hito.image
      },
      {
        merge: true,
      }
    )
  }
  updateVideo(hito: Hitos) {
    return this.db.collection(this.COLLECTION_NAME).doc(hito.id).set(
      {
        video: hito.video
      },
      {
        merge: true,
      }
    )
  }

  findAllHitos() {
    return this.hitos;
  }
  findCategories() {
    return this.db.collection<Categories>("category").valueChanges();
  }


  async onUploadImage(e: any) {
    const id = Math.random().toString(36).substring(2);
    const file = e.target.files[0];
    const filePath = `images-hitos/${id}`;
    const ref = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges().pipe(finalize(() => {
      ref.getDownloadURL().subscribe(urlImage => {
        this.hito.image = urlImage;
      });
    })
    ).subscribe();


  }
  onUploadVideo(e: any) {
    const id = Math.random().toString(36).substring(2);
    const file = e.target.files[0];
    const filePath = `videos-hitos/${id}`;
    const ref = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges().pipe(finalize(() => {
      ref.getDownloadURL().subscribe(urlVideo => {
        this.hito.video = urlVideo;
      });
    })
    ).subscribe();
    return this.hito.video;
  }



}
