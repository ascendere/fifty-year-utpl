import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { Degree, Inspiring } from './../@interfaces/inspiring';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class InspiringService {

  constructor(private db: AngularFirestore, private storage: AngularFireStorage) { }
  addInspiring(inspiring: Inspiring) {
    console.log(inspiring)
    return this.db.collection('inspiring').add(
      inspiring
    );

    /*  this.hitosCollection = this.db.collection(this.COLLECTION_NAME);
     this.hitosCollection.add(hito); */
  }

  getInspiring() {
    return this.db.collection<Inspiring>('inspiring').valueChanges()
  }
  getType(type: string) {
    return this.db.collection<Inspiring>('inspiring', ref => ref.where('type', '==', type)).valueChanges()
  }
  getInspiring2() {
    return this.db.collection<Inspiring>('inspiring').snapshotChanges()
  }
  getDegree() {
    return this.db.collection<Degree>('carreras').valueChanges()
  }

}
