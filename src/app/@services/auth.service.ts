import { RoleValidator } from './../validator/admin';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { User } from './../@interfaces/user';
import { AngularFireAuth } from '@angular/fire/auth';
import auth from 'firebase';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { Injectable, NgZone } from "@angular/core";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public userData: User;
  userState: any;
  public stateActive$: boolean;
  public user$: Observable<User>;

  admin = [
    "cacardenas7@utpl.edu.ec",
    "kdcoronel@utpl.edu.ec",
    "miloaiza@utpl.edu.ec",
    "lpuertas@utpl.edu.ec",
    "mclandacay@utpl.edu.ec",
    "javalladaresx@utpl.edu.ec",
    "mdvalarezo2@utpl.edu.ec",
    "jcmaldonado2@utpl.edu.ec",
    "psandrade@utpl.edu.ec",
    "ibarmijos@utpl.edu.ec",
    "mvlopezx@utpl.edu.ec",
  ]

  constructor(
    public ngZone: NgZone, // NgZone service to remove outside scope warning
    public afs: AngularFirestore,   // Inject Firestore service
    public afAuth: AngularFireAuth, // Inject Firebase auth service
    public router: Router,
  ) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userData = user;
        localStorage.setItem('user', JSON.stringify(this.userData));
        JSON.parse(localStorage.getItem('user'));
      } else {
        localStorage.setItem('user', null);
        JSON.parse(localStorage.getItem('user'));
      }
    })
  }
  SetUserData(result, rol) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(
      `users/${result.user.uid}`
    );
    return userRef.set(
      {
        ...result.additionalUserInfo.profile,
      },
      {
        merge: true,
      }
    );
  }
  // Returns true when user is looged in and email is verified
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user !== null) ? true : false;
  }

  // Returns true when user is looged in and email is verified
  get isAdmin(): boolean {
    const user = JSON.parse(localStorage.getItem('user')) as User;
    if(user == null){
      return false;
    }
    const exist = this.admin.indexOf(user.email)
    return (exist > 0) ? true : false;
  }

  MicrosoftAuth() {
    const provider = new auth.auth.OAuthProvider("microsoft.com");
    provider.setCustomParameters({
      client_id: environment.clientId,
      tenant: "6eeb49aa-436d-43e6-becd-bbdf79e5077d",
      redirect_uri: environment.redirectUri,
    });
    provider.addScope("user.read");
    provider.addScope("openid");
    provider.addScope("profile");
    return this.AuthLogin(provider);
  }
  AuthLogin(provider) {
    return this.afAuth
      .signInWithPopup(provider)
      .then((result) => {
        this.ngZone.run(async () => {
          const exist = this.admin.indexOf(result.user.email)
          if (exist < 0) {
            Swal.fire({
              title: "No autorizado",
              icon: "warning"
            })
          } else {
            this.SetUserData(result, "root").then(() => {
              this.router.navigate(['/admin']);
            })
          }
        });
      })
      .catch((error) => {
        console.log(JSON.parse(error));
        console.log(error.code);
        if (error.code === "auth/network-request-failed") {
          /* this.notification.showError(
            "Vuelva a intentar por favor",
            "La conexión de red es baja"
          ); */
        }
      });
  }
}
