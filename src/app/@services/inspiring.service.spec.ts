import { TestBed } from '@angular/core/testing';

import { InspiringService } from './inspiring.service';

describe('InspiringService', () => {
  let service: InspiringService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InspiringService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
