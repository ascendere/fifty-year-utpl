// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  clientId: 'ca8bac1b-5c55-41a3-84d0-50369903a083',
  redirectUri: 'http://localhost:4200/admin', // This is your redirect URI
  firebase: {
    apiKey: "AIzaSyCYE9kqx3rdJWChefbIW-X-AqkIny4faog",
    authDomain: "fifty-year-utpl.firebaseapp.com",
    projectId: "fifty-year-utpl",
    storageBucket: "fifty-year-utpl.appspot.com",
    messagingSenderId: "622922588226",
    appId: "1:622922588226:web:7c2b4151ebe068334c8ddb",
    measurementId: "G-1E076B06KG"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
