export const environment = {
  production: true,
  clientId: '06071308-bf52-4e43-8224-574feaef62d0',
  redirectUri: 'https://fifty-year-utpl.firebaseapp.com/admin', // This is your redirect URI
  firebase: {
    apiKey: "AIzaSyCYE9kqx3rdJWChefbIW-X-AqkIny4faog",
    authDomain: "fifty-year-utpl.firebaseapp.com",
    projectId: "fifty-year-utpl",
    storageBucket: "fifty-year-utpl.appspot.com",
    messagingSenderId: "622922588226",
    appId: "1:622922588226:web:7c2b4151ebe068334c8ddb",
    measurementId: "G-1E076B06KG"
  }
};
